package com.parmaia.myflickr.activities;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.loopj.android.image.SmartImageView;
import com.parmaia.myflickr.R;
import com.parmaia.myflickr.entities.Photo;
import com.parmaia.myflickr.utils.Favorites;
import com.parmaia.myflickr.utils.FlickrAPI;
import com.parmaia.myflickr.utils.MyFlickrApp;

public class FullScreenActivity extends ActionBarActivity implements FlickrAPI.FlickrUrlCallback{
  SmartImageView mImageView;
  Photo mPhoto;
  Favorites mFavorites;
  boolean mIsFavorite=false;
  enum GeoLocState{
    AVAILABLE, NOT_AVAILABLE, NOT_LOADED
  }
  GeoLocState mGeoLocState = GeoLocState.NOT_LOADED;
  private double mLongitude;
  private double mLatitude;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // remove title 
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    setContentView(R.layout.full_screen_photo);
    mPhoto=(Photo) getIntent().getSerializableExtra("PHOTO");
    mImageView=(SmartImageView) findViewById(R.id.photo);
    mImageView.setImageUrl(mPhoto.getUrlPhoto(),android.R.drawable.ic_delete, android.R.drawable.progress_indeterminate_horizontal);
    mFavorites = MyFlickrApp.getInstance().getFavorites();
    mIsFavorite = mFavorites.isFavorite(mPhoto);
    getGeoLocationInfo();
  }
  
  private void getGeoLocationInfo(){
    setSupportProgressBarIndeterminateVisibility(Boolean.TRUE);
    FlickrAPI.getGeoLocation(mPhoto.getId(), this);
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    //Inflate the menu items for use in the action bar
    MenuInflater inflater = getMenuInflater();
    switch(mGeoLocState){
    case AVAILABLE:
      inflater.inflate(R.menu.geolocacion_on, menu);
      break;
    case NOT_AVAILABLE:
      inflater.inflate(R.menu.geolocacion_off, menu);
      break;
    case NOT_LOADED:
      break;    
    }
    if (mIsFavorite)
      inflater.inflate(R.menu.favorite_on, menu);
    else
      inflater.inflate(R.menu.favorite_off, menu);
    
    return super.onCreateOptionsMenu(menu);
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
      // Handle presses on the action bar items
      switch (item.getItemId()) {
      case R.id.action_favorite_off:
        //Save to favorites
        mFavorites.addFavorite(mPhoto);
        mIsFavorite=true;
        supportInvalidateOptionsMenu();
        return true;
      case R.id.action_favorite_on:
        //Remove from favorites
        mFavorites.removeFavorite(mPhoto);
        mIsFavorite=false;
        supportInvalidateOptionsMenu();
        return true;
      case R.id.action_geolocation_on:
        openGoogleMaps();
        return true;
      default:
          return super.onOptionsItemSelected(item);
      }
  }
  
  private void openGoogleMaps(){
    String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f", mLatitude, mLongitude, mLatitude, mLongitude);
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    startActivity(intent);    
  }

  @Override
  public void onUrlResponse(String response) {
    Log.d("Full", response);
    try {
      JSONObject json = new JSONObject(response);
      if (json.getString("stat").equals("ok")){
        json = json.getJSONObject("photo").getJSONObject("location");
        mLatitude = json.getDouble("latitude");
        mLongitude = json.getDouble("longitude");
        mGeoLocState=GeoLocState.AVAILABLE;
      }else{
        mGeoLocState=GeoLocState.NOT_AVAILABLE;
      }
    } catch (JSONException e) {
      mGeoLocState=GeoLocState.NOT_AVAILABLE;
      e.printStackTrace();
    }
    
    setProgressBarIndeterminateVisibility(Boolean.FALSE);
    supportInvalidateOptionsMenu();
  }

  @Override
  public void onUrlError(Exception e) {
    setSupportProgressBarIndeterminateVisibility(Boolean.FALSE);
    mGeoLocState=GeoLocState.NOT_AVAILABLE;
    supportInvalidateOptionsMenu();
  }
}
