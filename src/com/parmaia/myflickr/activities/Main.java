package com.parmaia.myflickr.activities;

import java.util.Locale;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import com.parmaia.myflickr.R;
import com.parmaia.myflickr.fragments.FavoritesFragment;
import com.parmaia.myflickr.fragments.RecentsFragment;

public class Main extends ActionBarActivity implements OnClickListener, ViewPager.OnPageChangeListener {
  SectionsPagerAdapter mSectionsPagerAdapter;
  ViewPager mViewPager;
  CheckBox mBtnRecents, mBtnFavorites;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    mSectionsPagerAdapter = new SectionsPagerAdapter(
        getSupportFragmentManager());

    // Set up the ViewPager with the sections adapter.
    mViewPager = (ViewPager) findViewById(R.id.pager);
    mViewPager.setAdapter(mSectionsPagerAdapter);
    mViewPager.setOnPageChangeListener(this);
    mBtnRecents = (CheckBox) findViewById(R.id.btn_recents);
    mBtnFavorites = (CheckBox) findViewById(R.id.btn_favorites);
    mBtnRecents.setTag(0);
    mBtnFavorites.setTag(1);
    mBtnRecents.setOnClickListener(this);
    mBtnFavorites.setOnClickListener(this);
  }

  /**
   * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one
   * of the sections/tabs/pages.
   */
  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      return createFragment(position);
    }

    @Override
    public int getCount() {
      return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      Locale l = Locale.getDefault();
      switch (position) {
      case 0:
        return getString(R.string.title_recents).toUpperCase(l);
      case 1:
        return getString(R.string.title_favorites).toUpperCase(l);
      }
      return null;
    }
  }

  // Helper to create the corresponding fragment
  private Fragment createFragment(int position) {
    switch (position) {
    case 0: // Recents
      return RecentsFragment.getInstance();
    case 1: // Favorites
      return FavoritesFragment.getInstance();
    }
    return null;
  }

  private void toggleBtn(int tag){
    mBtnRecents.setChecked(tag==0);
    mBtnFavorites.setChecked(tag==1);
    mViewPager.setCurrentItem(tag, true);
  }
  
  @Override
  public void onClick(View v) {
    int tag = (Integer)v.getTag();
    toggleBtn(tag);
  }

  @Override
  public void onPageSelected(int position) {
    toggleBtn(position);    
  }
  
  @Override
  public void onPageScrollStateChanged(int arg0) {}

  @Override
  public void onPageScrolled(int arg0, float arg1, int arg2) {}

}
