package com.parmaia.myflickr.entities;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class Photo implements Serializable {
  private static final long serialVersionUID = 1L;

  private static String URL_PHOTO = "https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}{size}.jpg";
  
  private String mId;
  private String mSecret;
  private String mServer;
  private String mTitle;
  private String mFarm;
  
  public Photo(){
    
  }
  
  public Photo(String jsonStr){
    fromJson(jsonStr);
  }
  
  public void fromJson(String jsonStr){
    try {
      JSONObject json = new JSONObject(jsonStr);
      fromJson(json);
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  public void fromJson(JSONObject json){
    try {
      mId     = json.getString("id");
      mSecret = json.getString("secret");
      mServer = json.getString("server");
      mFarm   = json.getString("farm");
      mTitle  = json.getString("title");
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }
  
  public JSONObject toJSON(){
    JSONObject json = new JSONObject();
    try {
      json.put("id", mId);
      json.put("secret", mSecret);
      json.put("server", mServer);
      json.put("farm", mFarm);
      json.put("title", mTitle);
    } catch (JSONException e) {
      e.printStackTrace();
    }    
    return json;
  }
  
  public String getTitle(){
    return mTitle;
  }
  
  public String getUrlPhoto(){
    return getUrl("");
  }
  
  public String getUrlTumbnail(){
    return getUrl("_t");
  }
  
  private String getUrl(String size){
    String url = URL_PHOTO;
    url = url.replace("{farm-id}", mFarm);    
    url = url.replace("{server-id}", mServer);    
    url = url.replace("{id}", mId);    
    url = url.replace("{secret}", mSecret);
    url = url.replace("{size}", size);  
    return url;
  }
  
  public String getKey(){
    return mId+mSecret+mServer+mFarm;
  }

  public String getId() {
    return mId;
  }
  
}
