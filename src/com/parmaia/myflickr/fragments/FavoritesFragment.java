package com.parmaia.myflickr.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;
import com.parmaia.myflickr.R;
import com.parmaia.myflickr.activities.FullScreenActivity;
import com.parmaia.myflickr.entities.Photo;
import com.parmaia.myflickr.utils.MyFlickrApp;

public class FavoritesFragment extends Fragment {
  private static Fragment mInstance=null;
  ListView mList;
  FavoritesAdapter mFavoritesAdapter;
  View mNoFavoritesView;

  public static Fragment getInstance() {
    if (mInstance==null){
      mInstance=new FavoritesFragment();
    }
    return mInstance;
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.favorites, container, false);
    mList = (ListView) rootView.findViewById(R.id.listFavorites);
    mNoFavoritesView = rootView.findViewById(R.id.no_favorites);
    mFavoritesAdapter = new FavoritesAdapter(getActivity());
    mList.setAdapter(mFavoritesAdapter);
    return rootView;
  }
  
  @Override
  public void onResume() {
    if (mFavoritesAdapter.getCount()>0)
      mNoFavoritesView.setVisibility(View.GONE);
    else
      mNoFavoritesView.setVisibility(View.VISIBLE);
    super.onResume();
  }
  
  private class FavoritesAdapter extends BaseAdapter{
    private LayoutInflater inflater = null;
    
    public FavoritesAdapter(Context context){
      inflater = LayoutInflater.from(context);
    }
    
    @Override
    public int getCount() {
      return MyFlickrApp.getInstance().getFavorites().getList().size();
    }

    @Override
    public Object getItem(int pos) {
      return MyFlickrApp.getInstance().getFavorites().getList().get(pos);
    }

    @Override
    public long getItemId(int pos) {
      return pos;
    }

    private class ViewHolder{
      TextView tvTitle;
      SmartImageView imgThumbnail;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      final ViewHolder holder;
      if (convertView == null || convertView.getTag()==null){
         convertView = inflater.inflate(R.layout.thumbnail_item, null);
         holder = new ViewHolder();
         holder.tvTitle = (TextView) convertView.findViewById(R.id.title);
         holder.imgThumbnail = (SmartImageView) convertView.findViewById(R.id.thumbnail);
         convertView.setTag(holder);
      } else{
        holder = (ViewHolder) convertView.getTag();
      }
      final Photo photo = MyFlickrApp.getInstance().getFavorites().getList().get(position);
      holder.tvTitle.setText(photo.getTitle());
      
      holder.imgThumbnail.setImageUrl(photo.getUrlTumbnail(), android.R.drawable.ic_delete, android.R.drawable.progress_indeterminate_horizontal);
      convertView.setOnClickListener(new OnClickListener() {          
        @Override
        public void onClick(View v) {
          Intent i = new Intent(getActivity(), FullScreenActivity.class);
          i.putExtra("PHOTO", photo);
          startActivity(i);
        }
      });
      return convertView;
    }
  }
}
