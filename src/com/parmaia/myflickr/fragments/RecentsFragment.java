package com.parmaia.myflickr.fragments;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;
import com.parmaia.myflickr.R;
import com.parmaia.myflickr.activities.FullScreenActivity;
import com.parmaia.myflickr.components.RefreshableListView;
import com.parmaia.myflickr.entities.Photo;
import com.parmaia.myflickr.utils.FlickrAPI;
import com.parmaia.myflickr.utils.FlickrAPI.FlickrUrlCallback;
import com.parmaia.myflickr.utils.MyFlickrApp;

public class RecentsFragment extends Fragment implements FlickrUrlCallback, RefreshableListView.OnRefreshListener{
  private static Fragment mInstance=null;
  int mPage = 1;
  RefreshableListView mList;
  ThumbAdapter mThumbAdapter;
  boolean mPullToRefresh = false;

  public static Fragment getInstance() {
    if (mInstance==null){
      mInstance=new RecentsFragment();
    }
    return mInstance;
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.recent, container, false);
    mList = (RefreshableListView) rootView.findViewById(R.id.listThumbs);
    mThumbAdapter = new ThumbAdapter(getActivity());
    mList.setAdapter(mThumbAdapter);
    mList.setOnRefreshListener(this);
    mPage=1;
    loadRecents();
    return rootView;
  }
  
  private void loadRecents(){
    FlickrAPI.getRecents(10, mPage, this);
    //When done, call onAPIResponse on success, or onAPIError on error.
  }

  @Override
  public void onUrlResponse(String response) {
    Log.d("Recents", response);
    //Lets parse the response.
    try {
      JSONObject json = new JSONObject(response);
      if (json.getString("stat").equals("ok")){
//        if (page==1){
//          //If loading the first page, clear previous loaded items...
//          thumbAdapter.clear();        
//        }
        json = json.getJSONObject("photos");
        mPage = json.getInt("page")+1;//The next page to retrieve
        JSONArray photos = json.getJSONArray("photo");
        for (int i=0; i<photos.length();i++){
          Photo photo = new Photo();
          photo.fromJson(photos.getString(i));
          if(mPullToRefresh)
            mThumbAdapter.insert(photo);
          else
            mThumbAdapter.add(photo);
        }
        mThumbAdapter.notifyDataSetChanged();
        getView().findViewById(R.id.progressBar).setVisibility(View.GONE);
        mList.setVisibility(View.VISIBLE);
      }else{
        Toast.makeText(getActivity(), "Error parsing thumbnails", Toast.LENGTH_SHORT).show();
      }
    } catch (JSONException e) {
      Toast.makeText(getActivity(), "Error parsing thumbnails", Toast.LENGTH_SHORT).show();
      e.printStackTrace();
    }
    mList.completeRefreshing(true);
    mPullToRefresh=false;
  }

  @Override
  public void onUrlError(Exception e) {
    Toast.makeText(getActivity(), "Error getting thumbnails", Toast.LENGTH_SHORT).show();
    getView().findViewById(R.id.progressBar).setVisibility(View.GONE);
    mList.setVisibility(View.VISIBLE);
    e.printStackTrace();
  }

  private class ThumbAdapter extends BaseAdapter{
    Vector<Photo> items; //To store photo data
    Vector<String> keys; //To avoid duplicates
    private LayoutInflater inflater = null;
    int extraItem=1;//For the "loading..." item at the end of the list
    
    public ThumbAdapter(Context context){
      inflater = LayoutInflater.from(context);
      items = new Vector<Photo>();
      keys=new Vector<String>();
    }
    
    public void insert(Photo photo) {
      if (!keys.contains(photo.getKey())){
        items.insertElementAt(photo, 0);
        keys.insertElementAt(photo.getKey(), 0);
      }
    }

    public void add(Photo photo){
      if (!keys.contains(photo.getKey())){
        items.add(photo);
        keys.add(photo.getKey());
      }
    }
    
    @Override
    public int getCount() {
      return items.size()==0?0:items.size()+extraItem;
    }

    @Override
    public int getViewTypeCount() {
      return items.size()==0?1:1+extraItem;
    }
    
    @Override
    public int getItemViewType(int position) {
      return position<items.size()?0:1;
    }
    
    @Override
    public Object getItem(int pos) {
      return pos<items.size()?items.get(pos):"Loading more";
    }

    @Override
    public long getItemId(int pos) {
      return pos;
    }

    private class ViewHolder{
      TextView tvTitle;
      SmartImageView imgThumbnail;
      View favoriteIcon;
      //View progressBar;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      final ViewHolder holder;
      if (getItemViewType(position)==0){
        //Thumbnail item        
        if (convertView == null || convertView.getTag()==null){
           convertView = inflater.inflate(R.layout.thumbnail_item, null);
           holder = new ViewHolder();
           holder.tvTitle = (TextView) convertView.findViewById(R.id.title);
           holder.imgThumbnail = (SmartImageView) convertView.findViewById(R.id.thumbnail);
           holder.favoriteIcon = convertView.findViewById(R.id.favorite_icon);
           //holder.progressBar = convertView.findViewById(R.id.progressBar);
           convertView.setTag(holder);
        } else{
          holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(items.get(position).getTitle());
        
        /* This is for direct image load. It works but need to cache the image...
        FlickrAPI.loadImage(items.get(position).getUrlTumbnail(),new FlickrImgCallback() {          
          @Override
          public void onImgResponse(Bitmap bitmap) {
            holder.imgThumbnail.setImageBitmap(bitmap);
            holder.imgThumbnail.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.GONE);
          }
          
          @Override
          public void onImgError(Exception e) {
            holder.imgThumbnail.setImageResource(android.R.drawable.ic_delete);
            holder.imgThumbnail.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.GONE);
          }
        });
        ... instead use this SmartImageView component which has the cache incorporated.
        */
        if (MyFlickrApp.getInstance().getFavorites().isFavorite(items.get(position)))
          holder.favoriteIcon.setVisibility(View.VISIBLE);
        else
          holder.favoriteIcon.setVisibility(View.INVISIBLE);
        holder.imgThumbnail.setImageUrl(items.get(position).getUrlTumbnail(), android.R.drawable.ic_delete, android.R.drawable.progress_indeterminate_horizontal);
        convertView.setOnClickListener(new OnClickListener() {          
          @Override
          public void onClick(View v) {
            Intent i = new Intent(getActivity(), FullScreenActivity.class);
            i.putExtra("PHOTO", items.get(position));
            startActivity(i);
          }
        });
      }else{
        //Load more items item
        convertView = inflater.inflate(R.layout.loading_more_item, null);
        convertView.setTag(null);
        loadRecents();//Load the next page of images
      }
      return convertView;
    }
    
  }

  @Override
  public void onRefresh(RefreshableListView listView) {
    Log.d("Recents","Refreshing");
    mPage=1;
    mPullToRefresh=true;
    loadRecents();    
  }
}
