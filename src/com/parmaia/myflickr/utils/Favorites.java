package com.parmaia.myflickr.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.parmaia.myflickr.entities.Photo;

public class Favorites {
  private SharedPreference mPreferences;
  private Context mContext;
  private List<Photo> mFavoritesList;
  private List<String> mKeysList;

  public Favorites(Context context){
    this.mContext = context;
    mKeysList=new ArrayList<String>();
    mPreferences = new SharedPreference();
    mFavoritesList = mPreferences.getFavorites(context);
    updateKeys();
    //Uncomment this line to show a photo with geolocalization info in favorites.
    //addFavorite(new Photo("{\"id\": \"15817824689\", \"owner\": \"32975413@N06\", \"secret\": \"622b94f84d\", \"server\": \"8617\", \"farm\": 9, \"title\": \"My new workout sandbag from Goruck.\", \"ispublic\": 1, \"isfriend\": 0, \"isfamily\": 0 }"));
  }
  
  private void updateKeys(){
    mKeysList.clear();
    for(int i=0;i<mFavoritesList.size();i++){
      mKeysList.add(mFavoritesList.get(i).getKey());
    }
  }
  
  public void addFavorite(Photo photo){
    if(!mKeysList.contains(photo.getKey())){
      mFavoritesList.add(photo);
      mKeysList.add(photo.getKey());
      mPreferences.saveFavorites(mContext, mFavoritesList);
    }
  }
  
  public void removeFavorite(Photo photo){
    if(mKeysList.contains(photo.getKey())){
      int loc = mKeysList.indexOf(photo.getKey());
      mFavoritesList.remove(loc);
      mKeysList.remove(loc);
      mPreferences.saveFavorites(mContext, mFavoritesList);
    }
  }
  
  public boolean isFavorite(Photo photo){
    return mKeysList.contains(photo.getKey());
  }

  public List<Photo> getList(){
    return mFavoritesList;
  }

}
