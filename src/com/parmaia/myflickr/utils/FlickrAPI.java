package com.parmaia.myflickr.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class FlickrAPI {
  private static String FLICKR_API_URL        = "https://www.flickr.com/services/rest/";
  private static String FLICKR_METHOD_RECENTS = "flickr.photos.getRecent";
  private static String FLICKR_METHOD_GEOLOC  = "flickr.photos.geo.getLocation";
  private static String FLICKR_API_KEY_AUTH   = "api_key=c31540c36e54d6d13e79150a19f1bf64";
  private static String FLICKR_FORMAT_JSON    = "nojsoncallback=1&format=json";
  
  public interface FlickrUrlCallback{
    public void onUrlResponse(String response);
    public void onUrlError(Exception e);
  }
  public interface FlickrImgCallback{
    public void onImgResponse(Bitmap bitmap);
    public void onImgError(Exception e);
  }
 
  public static void getRecents(int perPage, int page, FlickrUrlCallback callback){
    String url= buildUrl(FLICKR_METHOD_RECENTS, "&per_page="+perPage+"&page="+page );
    getUrl(url, callback);
  }
  
  public static void getGeoLocation(String id, FlickrUrlCallback callback){
    String url = buildUrl(FLICKR_METHOD_GEOLOC, "&photo_id="+id);
    getUrl(url, callback);
  }
  
  private static String buildUrl(String method, String customParams){
    return FLICKR_API_URL+"?method="+method+"&"+FLICKR_API_KEY_AUTH+customParams+"&"+FLICKR_FORMAT_JSON;
  }
  
  private static void getUrl(final String urlStr, final FlickrUrlCallback callback){
    Log.d("FlickrUrl", urlStr);
    new AsyncTask<Void, Void, String>() {
      Exception err=null;
      @Override
      protected String doInBackground(Void... params) {
        try {
          String data;
          HttpClient httpclient = new DefaultHttpClient();
          HttpResponse response = httpclient.execute(new HttpGet(urlStr));
          BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
          StringBuffer sb = new StringBuffer("");
          String l = "";
          String nl = System.getProperty("line.separator");
          while ((l = in.readLine()) != null) {
              sb.append(l + nl);
          }
          in.close();
          data = sb.toString();
          return data;
        } catch (Exception e) {
          err=e;
        }
        return null;
      }
      
      protected void onPostExecute(String result) {
        if (result==null){
          callback.onUrlError(err);
        }else{
          callback.onUrlResponse(result);
        }
      }
    
    }.execute();
  }
  
  public static void loadImage(final String urlStr, final FlickrImgCallback callback){
    Log.d("FlickrImg", urlStr);
    new AsyncTask<Void, Void, Bitmap>() {
      Exception err=null;
      @Override
      protected Bitmap doInBackground(Void... params) {
        try { 
          Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(urlStr).getContent());
          return bitmap;
        } catch (Exception e) {
          err = e;
          return null;
        }
      }
      
      protected void onPostExecute(Bitmap result) {
        if (result==null){
          callback.onImgError(err);          
        }else{
          callback.onImgResponse(result);          
        }        
      }    
    }.execute();    
  }
  
}
