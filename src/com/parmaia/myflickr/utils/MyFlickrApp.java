package com.parmaia.myflickr.utils;

import android.app.Application;

public class MyFlickrApp extends Application{
  private Favorites mFavorites;
  
  private static MyFlickrApp mInstance;
  
  public static MyFlickrApp getInstance(){
    return mInstance;
  }
  
  public void onCreate(){
    super.onCreate();
    mInstance=this;
    mFavorites = new Favorites(this);
  }
  
  public Favorites getFavorites(){
    return mFavorites;
  }
  
}
