package com.parmaia.myflickr.utils;
 
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.parmaia.myflickr.entities.Photo;
 
public class SharedPreference {
 
    public static final String PREFS_NAME = "myFlickr";
    public static final String FAVORITES = "Favorite_Photos";
    
    public SharedPreference() {
        super();
    }
 
    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<Photo> favorites) {
        SharedPreferences settings;
        Editor editor;
 
        if (favorites!=null){
          settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
          editor = settings.edit();
   
          String jsonFavorites = toJson(favorites);
   
          editor.putString(FAVORITES, jsonFavorites);
   
          editor.commit();
        }
    }
 
    public ArrayList<Photo> getFavorites(Context context) {
        SharedPreferences settings;
        List<Photo> favorites=new ArrayList<Photo>();
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); 
        if (settings.contains(FAVORITES)) {
          favorites = new ArrayList<Photo>();
          String jsonFavorites = settings.getString(FAVORITES, null);
          fromJson(jsonFavorites, favorites);
        }
        return (ArrayList<Photo>) favorites;
    }

    private String toJson(List<Photo> favorites) {
      JSONArray json = new JSONArray();
      for (int i=0;i<favorites.size();i++){
        json.put(favorites.get(i).toJSON());
      }
      return json.toString();
    }

    private void fromJson(String jsonFavorites, List<Photo> favorites) {
      try {
        JSONArray json = new JSONArray(jsonFavorites);
        for(int i=0;i<json.length();i++){
          Photo photo = new Photo();
          photo.fromJson(json.getJSONObject(i));
          favorites.add(photo);
        }
      } catch (JSONException e) {        
        e.printStackTrace();
      }      
    }
}